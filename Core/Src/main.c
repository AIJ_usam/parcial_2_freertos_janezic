/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "semphr.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

#define Orden_FIR1 4  // Orden del primer FIR
#define Orden_FIR2 4   // Orden del segundo FIR


/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;

uint32_t Inst01 = 0, Inst02 = 0, InstIdle = 0;

SemaphoreHandle_t Semaforo_ADC;
SemaphoreHandle_t Semaforo_FiltroFIR;


QueueHandle_t QueueDatos;
QueueHandle_t QueueSalida;


osThreadId defaultTaskHandle;
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);

void StartDefaultTask(void const * argument);

uint32_t UNSAM_ADC (void);  // Prototipo API para leer datos ADC
void UNSAM_DAC (unsigned int); //Prototipo API salida datos a DAC

void Toma_Datos (void const*);
void Procesa_Datos(void const*);
void Salida_Datos(void const*);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */

 Semaforo_ADC = xSemaphoreCreateBinary();
 Semaforo_FiltroFIR = xSemaphoreCreateBinary();

 xSemaphoreGive ( Semaforo_ADC);    // Los semaforos arrancan dados
 xSemaphoreGive (Semaforo_FiltroFIR);


  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */


 Cola_Datos = xQueueCreate( Orden_FIR1 , sizeof(uint32_t) );
 Salida_Datos = xQueueCreate( Orden_FIR1 , sizeof(uint32_t) );



  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  xTaskCreate((void *)Toma_Datos(), "Toma", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 3, NULL); // Prioridad maxima
  xTaskCreate((void *)Procesa_Datos(), "Procesa", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 2, NULL); // Prioridad media
  xTaskCreate((void *) Salida_Datos(), "Salida", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL); // Prioridad baja

  /* Start scheduler */
 // osKernelStart();
 vTaskStartScheduler();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */

	void Toma_Datos(void const* argument)
	{
		uint32_t Datos[4];
		int i;

		while(1){

			xSemaphoreTake(Semaforo_ADC,portMAX_DELAY);  // Tomo el semáforo de esta tarea

		for (i=0; i< Orden_FIR1; i++)  // Ciclo for donde voy pasando los 4 datos a la cola

			{
			Datos = UNSAM_ADC(); // Cargo datos del ADC en una variable

			xQueueSend( Cola_Datos, &Datos[i], portMAX_DELAY );  // Envio el dato a la cola

			Datos[i] = 0;
			}
			xSemaphoreGive(Semaforo_ADC);  // Libero el semáforo de esta tarea
			xSemaphoreGive(Semaforo_FiltroFIR);  // Libero el semáforo de la tarea siguiente
			}

		vTaskDelay(10/ portTICK_RATE_MS); // Demora que genero para pasar un dato a 100Hz 1/100Hz = 10mSeg.

	}
	void Procesa_Datos (void const*)

	{

		unsigned int i;
		uint32_t  y1 = 0; // Salida FIR1
		uint32_t  y2 = 0; // Salida FIR2
		static uint32_t  Muestras [Orden_FIR1] = {0};
		static int x [Orden_FIR1] = {0};
		uint32_t y2aux [Orden_FIR2] = {0};
		static int pointer = (Orden_FIR1-1);
		unsigned int auxiliar;
		float b0 [Orden_FIR1] = {0.25, 0.3, 0.35, 0.4}; // Coeficintes del filtro FIR1 circular
		float b1 [Orden_FIR2] = { ((float) 1) / Orden_FIR2, ((float) 1) / Orden_FIR2 , ((float) 1) / Orden_FIR2 , ((float) 1) / Orden_FIR2}; // Coeficintes del filtro FIR2 lineal de media movil

		while(1){

			xSemaphoreTake(Semaforo_FiltroFIR,portMAX_DELAY);  // Tomo el semáforo de esta tarea


		for (i=0; i< Orden_FIR; i++)  // Ciclo for donde voy pasando los 4 datos de la cola a un vector

			{

			xQueueReceive( Cola_Datos, &Muestras[i], portMAX_DELAY );

			}

			x [pointer] = Muestra [pointer];  // Paso las muestras al vector X, entrda del filtro FIR

		for (i = pointer; i < (pointer + Orden_FIR1) ; i ++ )  // Hago el filtrados
				{
					y1 += x[i % Orden_FIR1] * b0[i-pointer];  // Salida
				}

				pointer = abs((pointer - 1) % Orden_FIR);

				y2aux[i]= y1;

		for (i=0; j< Orden_FIR2; j++)  // Implementación FIR2

				   {
				      y2 = y2 + y2aux[j]*b1[j];
				   }

				{
						xQueueSend( Cola_Salida, &y2, portMAX_DELAY );
						y2=0;
				}

		xSemaphoreGive(Semaforo_FiltroFIR); // Libero semaforo de esta tarea

				}
	}

	void Salida_Datos(void const*)
	{
		unsigned int Salida;

			while(1){

			xQueueReceive( Cola_Salida, &Salida, portMAX_DELAY ); // Paso de la cola y a la variable Salida
			UNSAM_DAC(Salida); // Funcion o API que pasa la variable al DAC

			}
		}
	void StartDefaultTask(void const * argument)
	{

  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END 5 */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
